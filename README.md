# Alpha_ED

Alpha_ED is a concept which will help the students to learn better through monitoring their attention status and help teachers improve their learning and teaching experience.

Team Members:

`Bibek Lama` `Rijen Manandhar` `Sasmita Gurung` `Ishika Pradhan` `Roshan Gurung`

# Motto
We believe in learning. Teaching students with low attention level is just training, its not learning. We encourage to teach students when they have higher attention level.

## Project Requirements
pip install -r requirements.txt